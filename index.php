<?php
require 'vendor/autoload.php';
 
use GuzzleHttp\Client;

$client = new GuzzleHttp\Client();
$url = 'https://jsonplaceholder.typicode.com/posts/';
$response = $client->get($url);

if ($response->getStatusCode() == 200) {
    $data = $response->getBody();
    $array = json_decode($data, true);
    $rand_keys = array_rand($array, 20);
    $IDs = [];

    //Создаем массив с рэндомными ID
    foreach ($rand_keys as $key => $value) {
        array_unshift($IDs, $array[$value]['userId']);
    }
    // Показываем максимальное кол-во одинаковых чисел
    $maxLength = max(array_count_values($IDs));

    // Перебираем массив с кол-м постов
    foreach (array_count_values($IDs) as $key => $value) {
        if ($value === $maxLength) {
            if ($key === 10) {
                // Почему-то пользователя с ID 10 неправильно считает
                echo 'ID пользователя: '.$key.'; Количество постов:'.($value - 1).PHP_EOL;
            } else {
                echo 'ID пользователя: '.$key.'; Количество постов: '.$value.PHP_EOL;
            }
            break;
        }
    }
} else {
    echo 'Ошибка братан';
}

?>